class MoviesController < ApplicationController

  before_action :authenticate_user!, only: [:show, :new, :edit, :create, :update, :destroy, :like, :hate]

  def index
    sort_params = {'by_date' => 'movies.created_at', 'by_title' => 'movies.title', 'by_likes' => 'likes DESC', 'by_hates' => 'hates DESC'}

    filter_params = {'by_user' => 'movies.user_id'}
    whereQuery = ''
    if where_params.has_key?(:filter) && where_params.has_key?(:userid)
      whereQuery = filter_params[where_params[:filter]] + ' = ' + where_params[:userid]
    end
    # @movies = Movie.joins(:votes).where(whereQuery).ordered(sort_params[order_params[:order]])
    @movies = Movie.joins("LEFT JOIN `votes` ON votes.movie_id = movies.id").joins(:user)
                  .select("movies.*, sum(CASE WHEN votes.value = 1 THEN 1 ELSE 0 END) as likes, sum(CASE WHEN votes.value = 0 THEN 1 ELSE 0 END) as hates, users.name as user_name")
                  .where(whereQuery)
                  .group("movies.id")
                  .ordered(sort_params[order_params[:order]])
    @votes = Hash.new

    @movies.each do |movie|
      likesHash = Hash.new
      hatesHash = Hash.new
      votesArray = movie.votes

      votesArray.each do |vote|
        if vote.value == 1
          likesHash[vote.user_id] = 1
        else
          hatesHash[vote.user_id] = 0
        end
      end
      @votes[movie.title] = Hash['likes' => likesHash, 'hates' => hatesHash]
    end
  end

  def show
    @movie = Movie.find(params[:id])
  end

  def new
    @movie = Movie.new
  end

  def edit
    @movie = Movie.find(params[:id])
  end

  def create
    @movie = Movie.new(movie_params)

    if @movie.save
      flash[:success] = 'Movie was successfully created'
      redirect_to @movie
    else
      render 'new'
    end
  end

  def update
    @movie = Movie.find(params[:id])

    if @movie.update(movie_params)
      flash[:success] = 'Movie was successfully updated'
      redirect_to @movie
    else
      render 'edit'
    end
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:success] = 'Movie was successfully Removed'
    redirect_to movies_path
  end

  def like
    # Get Movie model
    movie = Movie.find(params[:movieid])

    # User cannot vote for his movies
    if current_user.id != movie.user_id

      # Search for likes if exist unlike them
      like = Vote.find_by user: current_user, movie: movie, value: 1
      # Search for hates if exist delete them
      hate = Vote.find_by user: current_user, movie: movie, value: 0

      if like
        like.destroy
        flash[:success] = 'You Unlike the movie '+movie.title
      else
        like = Vote.new(user: current_user, movie: movie, value: 1)
        if like.save
          if hate
            hate.destroy
          end
          flash[:success] = 'You Like the movie '+movie.title
        else
          flash[:danger] = 'Failed to Like the movie'+movie.title
        end
      end
    end
    redirect_to movies_path
  end

  def hate
    # Get Movie model
    movie = Movie.find(params[:movieid])

    # User cannot vote for his movies
    if current_user.id != movie.user_id

      # Search for hates if exist unhate them
      hate = Vote.find_by user: current_user, movie: movie, value: 0
      # Search for likes if exist unlike them
      like = Vote.find_by user: current_user, movie: movie, value: 1

      if hate
        hate.destroy
        flash[:success] = 'You Unhate the movie '+movie.title
      else
        hate = Vote.new(user: current_user, movie: movie, value: 0)
        if hate.save
          if like
            like.destroy
          end
          flash[:success] = 'You Hate the movie '+movie.title
        else
          flash[:danger] = 'Failed to Hate the movie'+movie.title
        end
      end
    end
    redirect_to movies_path
  end

  private
  def movie_params
    params.require(:movie).permit(:title, :description, :user).merge(user_id: current_user.id)
  end

  def order_params
    params.permit(:order);
  end

  def where_params
    params.permit(:filter, :userid);
  end
end
