class Movie < ActiveRecord::Base
  has_many :votes
  belongs_to :user
  validates :title, presence: true,
            length: { minimum: 2 }
  scope :ordered, ->(field) { order(field || 'created_at DESC') }
end
