class RemoveCreatorcolumnFromMovies < ActiveRecord::Migration
  def change
    remove_column :movies, :creator_id, :integer
  end
end
